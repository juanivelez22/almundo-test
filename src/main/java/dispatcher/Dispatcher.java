package dispatcher;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import empleados.Employee;
import llamada.Call;

public class Dispatcher{
	// Lista de empleados
	private List<Employee> employees = Collections.synchronizedList(new ArrayList<Employee>());
	// Lista de llamadas
	private List<Call> calls = new ArrayList<Call>();
	
	// Metodo para ingresar llamadas
	public void dispatchCall(Call l) throws InterruptedException{
		addCall(l);
		Thread t = new Thread(l);
		t.start();
	}
	
	// Agregar una llamada a la cola.
	public synchronized void addCall(Call i){
		calls.add(i);		
		System.out.println("Incoming Call ID "+i.getID());
	}
	// Devuelve el primer empleado libre segun el orden planteado en el problema (Operador-Supervisor-Director)
	public Employee getFreeEmployee(){
		synchronized (employees) {
			Collections.sort(employees);
			Iterator<Employee> it = employees.iterator();
			while (it.hasNext()){
				Employee employee = (Employee)it.next();
				if (!employee.isBusy()){
					employee.occupy();
					return employee;
				}
			}
			return null;
		}		
	}
	// Setea la lista de empleados
	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}
	
}
