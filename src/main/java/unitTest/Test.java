package unitTest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import dispatcher.Dispatcher;
import empleados.Director;
import empleados.Employee;
import empleados.Operator;
import empleados.Supervisor;
import llamada.Call;

public class Test {
	
	

	public static void main(String[] args) throws InterruptedException {
		
		Dispatcher dispatcher = new Dispatcher();
		//List<Empleado> empleados = new ArrayList<Empleado>();
		List<Employee> empleados = Collections.synchronizedList(new ArrayList<Employee>());
				
		Director dir = new Director("Director", dispatcher);
		Supervisor sup1 = new Supervisor("Supervisor 1", dispatcher);
		Supervisor sup2 = new Supervisor("Supervisor 2", dispatcher);
		Supervisor sup3 = new Supervisor("Supervisor 3", dispatcher);
		Operator oper1 = new Operator("Operador 1", dispatcher);
		Operator oper2 = new Operator("Operador 2", dispatcher);
		Operator oper3 = new Operator("Operador 3", dispatcher);
		Operator oper4 = new Operator("Operador 4", dispatcher);
		Operator oper5 = new Operator("Operador 5", dispatcher);
		Operator oper6 = new Operator("Operador 6", dispatcher);
		
		empleados.add(dir);
		empleados.add(sup1);
		empleados.add(sup2);
		empleados.add(sup3);
		empleados.add(oper1);
		empleados.add(oper2);
		empleados.add(oper3);
		empleados.add(oper4);
		empleados.add(oper5);
		empleados.add(oper6);
				
		dispatcher.setEmployees(empleados);
				
		for (int i = 0; i < 20; i++) {
			dispatcher.dispatchCall(new Call(i, dispatcher));
		}		
		
	}

}
