package llamada;
import java.util.Random;
import dispatcher.Dispatcher;
import empleados.Employee;

public class Call implements Runnable{

	private int maxDuration = 10;
	private int minDuration = 5;
	private Dispatcher dispatcher;
	private Integer id;
	private boolean answered;
	
	public Call(Integer id,Dispatcher d){
		this.id = id;
		dispatcher = d;
		answered = false;
	}
	// Devuelve el ID de la llamada.
	public int getID(){
		return id;
	}
	// Marca como atendida la llamada
	public synchronized void answer(){
		answered = true;		
	}
	// Devuelve si la llamada fue atendida o no
	public boolean answered(){
		return answered;
	}
	
	@Override
	public void run() {
		while (!answered){
			Random r = new Random();
			int seconds = r.nextInt(maxDuration-minDuration) + minDuration;
			Employee emp = dispatcher.getFreeEmployee();
			if (emp != null){				
				System.out.println("CallID "+this.id+"-"+emp.getName()+" "+seconds+" segs.");
				try {
					Thread.sleep(seconds*1000);
					answer();
				} catch (InterruptedException e) {
					System.out.println("->Interruption");
				}
				emp.unoccupy();				
			}else{
				synchronized (dispatcher) {
					try {
						System.out.println("CallID "+id+" waiting...");
						dispatcher.wait();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}

}
