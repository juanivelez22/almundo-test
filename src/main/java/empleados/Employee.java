package empleados;
import dispatcher.Dispatcher;

public abstract class Employee implements Comparable<Employee>  {
	
	private Dispatcher dispatcher;
	private String name;
	private boolean busy;
	
	public Employee(String name, Dispatcher d){
		this.name = name;
		busy = false;
		dispatcher = d;
	}
	// Ocupa al empleado.
	public synchronized void occupy(){
		busy = true;
		System.out.println("Answer "+getName());
	}
	// Desocupa al empleado
	public synchronized void unoccupy(){
		busy = false;
		System.out.println("Hang up "+getName());
		synchronized (dispatcher) {
			dispatcher.notifyAll();
		}		
	}
	// Devuelve si el empleado esta ocupado 
	public synchronized boolean isBusy(){
		return busy;
	}
	// Devuelve el nombre del empleado
	public String getName() {
		return name;
	}
	// Devuelve el tipo de empleado
	public abstract Integer getEmployeeType();
	
}	