package empleados;

import dispatcher.Dispatcher;

public class Director extends Employee {
	// Tipo de empleado Director = 3
	public final static int employeeType = 3;
	
	public Director(String name, Dispatcher d) {
		super(name,d);
	}
	
	@Override
	// Compara empleados por tipo.
	public int compareTo(Employee o) {
		return getEmployeeType().compareTo(o.getEmployeeType());
	}
	
	public Integer getEmployeeType(){
		return employeeType;
	}
	
}
