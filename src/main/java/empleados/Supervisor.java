package empleados;

import dispatcher.Dispatcher;

public class Supervisor extends Employee {

	// Tipo de empleado Supervisor = 2
	public final static int employeeType = 2;
	
	public Supervisor(String name, Dispatcher d) {
		super(name,d);
	}
		
	@Override
	// Compara empleados por tipo.
	public int compareTo(Employee o) {
		return getEmployeeType().compareTo(o.getEmployeeType());
	}
	
	public Integer getEmployeeType(){
		return employeeType;
	}

}
