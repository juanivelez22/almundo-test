package empleados;

import dispatcher.Dispatcher;

public class Operator extends Employee {
	// Tipo de empleado Operador = 1
	public final static int employeeType = 1;
	
	public Operator(String name, Dispatcher d) {
		super(name,d);
	}
	
	@Override
	// Compara empleados por tipo.
	public int compareTo(Employee o) {
		return getEmployeeType().compareTo(o.getEmployeeType());
	}
	
	public Integer getEmployeeType(){
		return employeeType;
	}
	
}
